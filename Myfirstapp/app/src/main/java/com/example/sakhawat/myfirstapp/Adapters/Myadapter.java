package com.example.sakhawat.myfirstapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sakhawat.myfirstapp.Models.PhoneData;
import com.example.sakhawat.myfirstapp.R;

import java.util.ArrayList;

public class Myadapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<PhoneData> items;
    private OnItemClickListener mListener;

    public Myadapter(Context context, ArrayList<PhoneData> items) {
        this.context = context;
        this.items = items;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View row = layoutInflater.inflate(R.layout.custom_row, viewGroup, false);
        Item item = new Item(row,mListener);
        return item;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int i) {
        final PhoneData temp = items.get(i);
        ((Item) viewHolder).textname.setText(temp.getName());
        ((Item) viewHolder).textphn.setText(temp.getPhoneno());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener{
        void onItemClickListener(int pos);
    }
    public void setOnItemClickListener(OnItemClickListener listener)
    {
        mListener=listener;
    }

    public static class Item extends RecyclerView.ViewHolder {
        TextView textname,textphn;
        ImageView imageView;

        public Item(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            textname = itemView.findViewById(R.id.tvname);
            textphn=itemView.findViewById(R.id.tvphn);
            imageView = itemView.findViewById(R.id.call);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null)
                    {
                        int pos=getAdapterPosition();
                        if(pos!= RecyclerView.NO_POSITION);
                        {
                            listener.onItemClickListener(pos);
                        }
                    }
                }
            });
        }
    }
}
