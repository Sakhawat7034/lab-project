package com.example.sakhawat.myfirstapp;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sakhawat.myfirstapp.Adapters.Myadapter;
import com.example.sakhawat.myfirstapp.Models.PhoneData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class GetDataFromAccount extends AppCompatActivity {
    RecyclerView recyclerView;
    TextView textView;
    Myadapter mAdapter;
    ProgressBar progressBar;
    ArrayList<PhoneData> mylist = new ArrayList<>();
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    Boolean result;

      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_data_from_account);
        UISetting();
        checkpremission();

    }

    private void UISetting() {
        recyclerView = findViewById(R.id.rvsshowonlinedata);
        textView = findViewById(R.id.tvmessage);
        progressBar = findViewById(R.id.progressBar2);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
    }

    private class DataFromFirebase extends AsyncTask<String,Void,String> {


        @Override
        protected String doInBackground(String... strings) {
            String temp;
            getPhoneNumberFromFirebase();
            if(mylist.size()>0)
            {
                temp="Mylist has data";
            }
            else {
                temp="Mylist has no data";
            }
            return temp;
        }


        private void getPhoneNumberFromFirebase() {
            DatabaseReference databaseReference = firebaseDatabase.getReference(firebaseAuth.getUid());

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child("mylis").getValue() == null) {
                        progressBar.setVisibility(View.INVISIBLE);
                        textView.setVisibility(View.VISIBLE);
                        textView.setText("Data not yet Syncronized");


                    } else {
                        for (DataSnapshot dsp : dataSnapshot.child("mylis").getChildren()) {
                            mylist.add(dsp.getValue(PhoneData.class));

                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        mAdapter=new Myadapter(getApplicationContext(),mylist);
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.setOnItemClickListener(new Myadapter.OnItemClickListener() {
                            @Override
                            public void onItemClickListener(int pos) {
                                Intent call = new Intent(Intent.ACTION_CALL);
                                call.setData(Uri.parse("tel:" + mylist.get(pos).getPhoneno())).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    return;
                                }
                                startActivity(call);
                            }
                        });

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }

            });

        }
    }
    private void checkpremission() {

        if(ContextCompat.checkSelfPermission(GetDataFromAccount.this, android.Manifest.permission.CALL_PHONE)== PackageManager.PERMISSION_GRANTED)
        {
            new DataFromFirebase().execute("");
        }
        else {
            requestContactsPremission();
        }
    }

    private void requestContactsPremission() {

        if(ActivityCompat.shouldShowRequestPermissionRationale(GetDataFromAccount.this, android.Manifest.permission.CALL_PHONE))
        {
            new AlertDialog.Builder(GetDataFromAccount.this).setTitle("permission needed ")
                    .setMessage("This permission is needed to syncronise your contacts number and  name")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(GetDataFromAccount.this,new String[] {
                                    android.Manifest.permission.CALL_PHONE},7034);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).create().show();

        }else {
            ActivityCompat.requestPermissions(GetDataFromAccount.this,new String[] { Manifest.permission.CALL_PHONE},7034);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==7034)
        {
            if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
            {
                new DataFromFirebase().execute("");
            }
            else {
                Toast.makeText(getApplicationContext(),"Please grant call permission",Toast.LENGTH_LONG).show();
                finish();

            }
        }

    }
}