package com.example.sakhawat.myfirstapp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
<<<<<<< HEAD
=======
import android.os.Build;
>>>>>>> master
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sakhawat.myfirstapp.Adapters.Myadapter;
import com.example.sakhawat.myfirstapp.Models.PhoneData;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

public class MainActivity2 extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    Myadapter mAdapter;
    ArrayList<PhoneData> mylis = new ArrayList<>();
    EditText name, roll;
    TextView tv;
    Button show;
    String namecsv = "";
    String phonecsv = "";
    HashSet<String> dupset = new HashSet<>();
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    Boolean re = true;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        UIsetting();
        getDataFromPhoneBook();


    }

    private boolean getPhoneNumberFromFirebase() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference(firebaseAuth.getUid());

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("mylis").getValue() == null) {
                    Toast.makeText(getApplicationContext(), "data did not get", Toast.LENGTH_LONG).show();
                } else {
                    ArrayList<String> lst = new ArrayList<>();
                    for (DataSnapshot dsp : dataSnapshot.child("mylis").getChildren()) {
                        lst.add(dsp.getValue().toString());
                    }
                    re = false;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Toast.makeText(getApplicationContext(), databaseError.getCode(), Toast.LENGTH_SHORT).show();

            }
        });
        return re;
    }

    private void UIsetting() {
        firebaseAuth = FirebaseAuth.getInstance();
        mRecyclerView = findViewById(R.id.rv1);
        firebaseDatabase = FirebaseDatabase.getInstance();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        databaseReference = firebaseDatabase.getReference(firebaseAuth.getUid());
    }

    private void getDataFromPhoneBook() {
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            if (name != null) {
                if (dupset.add(name)) {
                    mylis.add(new PhoneData(name, phoneNumber));
                }
            }

        }
        phones.close();

        Collections.sort(mylis, new Comparator<PhoneData>() {
            @Override
            public int compare(PhoneData o1, PhoneData o2) {
<<<<<<< HEAD
                return o1.getName().compareTo(o2.getName());
=======
                return o1.name.compareTo(o2.name);
>>>>>>> master
            }
        });

        Toast.makeText(getApplicationContext(), mylis.get(0).getName(), Toast.LENGTH_LONG).show();
        mAdapter = new Myadapter(getApplicationContext(), mylis);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new Myadapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int pos) {
                Intent call = new Intent(Intent.ACTION_CALL);
                call.setData(Uri.parse("tel:" + mylis.get(pos).getPhoneno())).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(call);

            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.draw_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.sync:
                databaseReference.child("mylis").setValue(mylis);
                Toast.makeText(getApplicationContext(),"successfull"+mylis.toString(),Toast.LENGTH_LONG).show();
                break;


        }

        return super.onOptionsItemSelected(item);
    }
}
